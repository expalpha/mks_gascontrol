/*******************************************************************\

  Name:         tmfe_example_everything.cxx
  Created by:   K.Olchanski

  Contents:     Example Front end to demonstrate all functions of TMFE class

\********************************************************************/

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <math.h> // M_PI

#include "midas.h"
#include "tmfe.h"
#include "MKS647C.hh"

void mks_callback(INT hDB, INT hkey, INT index, void *feptr);

class EqMKS :
   public TMFeEquipment
{
private:
   MKS647C mks;
   vector<int> status;
   vector<double> flow, flow_setrb, flow_set, flow_ratio;
public:
   EqMKS(const char* eqname, const char* eqfilename) // ctor
      : TMFeEquipment(eqname, eqfilename)
   {
      printf("EqMKS::ctor!\n");

      // configure the equipment here:

      //fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 1;
      fEqConfPeriodMilliSec = 1000;
      fEqConfLogHistory = 1;
      fEqConfWriteEventsToOdb = true;
      fEqConfEnablePoll = true; // enable polled equipment
      //fEqConfPollSleepSec = 0; // to create a "100% CPU busy" polling loop, set poll sleep time to zero
   }

   ~EqMKS() // dtor
   {
      printf("EqMKS::dtor!\n");
   }

   void HandleUsage()
   {
      printf("EqMKS::HandleUsage!\n");
   }

   TMFeResult HandleInit(const std::vector<std::string>& args)
   {
      printf("EqMKS::HandleInit!\n");
      fMfe->DeregisterTransitions();
      fEqConfReadOnlyWhenRunning = false; // overwrite ODB Common RO_RUNNING to false
      fEqConfWriteEventsToOdb = true; // overwrite ODB Common RO_ODB to true
      fOdbEqVariables->RIA("Status", &status, true, 8);
      fOdbEqVariables->RDA("Flow", &flow, true, 8);
      fOdbEqVariables->RDA("FlowSet", &flow_setrb, true, 8);
      fOdbEqVariables->RDA("FlowRatio", &flow_setrb, true, 8);
      fOdbEqSettings->RDA("FlowSet", &flow_set, true, 8);
      EqSetStatus("Started...", "white");
      if(mks.Connect(true)){
         EqSetStatus("Connected.", "lightgreen");

         flow_setrb = mks.GetFlowSetp();
         fOdbEqVariables->WDA("FlowSet", flow_setrb);
         string keypath = "Equipment/" + fEqName + "/Settings";
         HNDLE hkey;
         if(db_find_link(fMfe->fDB, 0, keypath.c_str(), &hkey) == DB_SUCCESS){
            KEY key;
            db_get_key(fMfe->fDB, hkey, &key);
            db_watch(fMfe->fDB, hkey, mks_callback, (void*)this);
            printf("Hotlink on for key %s\n", key.name);
         } else {
            printf("Can't watch directory\n");
         }

         return TMFeOk();
      } else
         return TMFeErrorMessage("Couldn't connect to device.");
   }

   TMFeResult HandleRpc(const char* cmd, const char* args, std::string& response)
   {
      fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);

      // RPC handler

      char tmp[256];
      time_t now = time(NULL);
      sprintf(tmp, "{ \"current_time\" : [ %d, \"%s\"] }", (int)now, ctime(&now));

      response = tmp;

      return TMFeOk();
   }

   // TMFeResult HandleBeginRun(int run_number)
   // {
   //    fMfe->Msg(MINFO, "HandleBeginRun", "Begin run %d!", run_number);
   //    EqSetStatus("Running", "#00FF00");
   //    return TMFeOk();
   // }

   // TMFeResult HandleEndRun(int run_number)
   // {
   //    fMfe->Msg(MINFO, "HandleEndRun", "End run %d!", run_number);
   //    EqSetStatus("Stopped", "#FFFFFF");
   //    return TMFeOk();
   // }

   // TMFeResult HandlePauseRun(int run_number)
   // {
   //    fMfe->Msg(MINFO, "HandlePauseRun", "Pause run %d!", run_number);
   //    EqSetStatus("Paused", "#FFFF00");
   //    return TMFeOk();
   // }

   // TMFeResult HandleResumeRun(int run_number)
   // {
   //    fMfe->Msg(MINFO, "HandleResumeRun", "Resume run %d!", run_number);
   //    EqSetStatus("Running", "#00FF00");
   //    return TMFeOk();
   // }

   // TMFeResult HandleStartAbortRun(int run_number)
   // {
   //    fMfe->Msg(MINFO, "HandleStartAbortRun", "Begin run %d aborted!", run_number);
   //    EqSetStatus("Stopped", "#FFFFFF");
   //    return TMFeOk();
   // }

   // void SendData(double dvalue)
   // {
   //    char buf[1024];
   //    ComposeEvent(buf, sizeof(buf));
   //    BkInit(buf, sizeof(buf));

   //    double* ptr = (double*)BkOpen(buf, "data", TID_DOUBLE);
   //    *ptr++ = dvalue;
   //    BkClose(buf, ptr);

   //    EqSendEvent(buf);
   // }

   void HandlePeriodic()
   {
      printf("EqMKS::HandlePeriodic!\n");

      status = mks.GetStatus();
      fOdbEqVariables->WIA("Status", status);
      flow = mks.GetFlows();
      fOdbEqVariables->WDA("Flow", flow);
      if(flow_ratio.size() != flow.size()) flow_ratio.resize(flow.size());
      for(unsigned int i = 0; i < flow.size(); i++){
         if(flow_set[i] == 0){
            flow_ratio[i] = 1.;
         } else {
            flow_ratio[i] = flow[i]/flow_set[i];
         }
      }
      fOdbEqVariables->WDA("FlowRatio", flow_ratio);
      flow_setrb = mks.GetFlowSetp();
      fOdbEqVariables->WDA("FlowSet", flow_setrb);
      // char status_buf[256];
      // sprintf(status_buf, "value %.1f", data);
      // EqSetStatus(status_buf, "#00FF00");
   }

   // bool HandlePoll()
   // {
   //    //printf("EqMKS::HandlePoll!\n");

   //    if (!fMfe->fStateRunning) // only poll when running
   //       return false;

   //    double r = drand48();
   //    if (r > 0.999) {
   //       // return successful poll rarely
   //       printf("EqMKS::HandlePoll!\n");
   //       return true;
   //    }

   //    return false;
   // }

   // void HandlePollRead()
   // {
   //    printf("EqMKS::HandlePollRead!\n");

   //       char buf[1024];

   //       ComposeEvent(buf, sizeof(buf));
   //       BkInit(buf, sizeof(buf));

   //       uint32_t* ptr = (uint32_t*)BkOpen(buf, "poll", TID_UINT32);
   //       for (int i=0; i<16; i++) {
   //          *ptr++ = lrand48();
   //       }
   //       BkClose(buf, ptr);

   //       EqSendEvent(buf, false); // do not write polled data to ODB and history
   // }

   void fecallback(HNDLE hkey, INT index){
      KEY key;
      int status = db_get_key(fMfe->fDB, hkey, &key);
      if(status == DB_SUCCESS){
         printf("ODB entry %s changed!\n", key.name);
      } else {
         printf("ODB error!\n");
         return;
      }

      if(std::string(key.name) == std::string("FlowSet")){
         fOdbEqSettings->RDA(key.name, &flow_set);
         printf("Flow Setting %d changed to %f!\n", index, flow_set[index]);
         mks.SetFlow(index + 1, flow_set[index]);
      } else {
         printf("Unknown key %s\n", key.name);
      }
   }
};

// example frontend

class FeMKS: public TMFrontend
{
public:
   FeMKS() // ctor
   {
      printf("FeMKS::ctor!\n");
      FeSetName("feMKS");
      FeAddEquipment(new EqMKS("GasMixer", __FILE__));
   }

   void HandleUsage()
   {
      printf("FeMKS::HandleUsage!\n");
   };

   TMFeResult HandleArguments(const std::vector<std::string>& args)
   {
      printf("FeMKS::HandleArguments!\n");
      return TMFeOk();
   };

   TMFeResult HandleFrontendInit(const std::vector<std::string>& args)
   {
      printf("FeMKS::HandleFrontendInit!\n");
      return TMFeOk();
   };

   TMFeResult HandleFrontendReady(const std::vector<std::string>& args)
   {
      printf("FeMKS::HandleFrontendReady!\n");
      //FeStartPeriodicThread();
      //fMfe->StartRpcThread();
      return TMFeOk();
   };

   void HandleFrontendExit()
   {
      printf("FeMKS::HandleFrontendExit!\n");
   };
};


void mks_callback(INT hDB, INT hkey, INT index, void *feptr)
{
   EqMKS* fe = (EqMKS*)feptr;
   fe->fecallback(hkey, index);
}

// boilerplate main function

int main(int argc, char* argv[])
{
   FeMKS fe_mks;
   return fe_mks.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
