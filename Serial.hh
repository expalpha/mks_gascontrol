#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <dirent.h>
#include <algorithm>

using std::string;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;

class SerialPort
{
public:
  SerialPort(tcflag_t iflag = 0, tcflag_t oflag = 0, tcflag_t cflag = CS8|CREAD|CLOCAL, tcflag_t lflag = 0, uint8_t timeout = 1, speed_t speed = B9600, string eol_ = "\r\n"): eol(eol_){
    memset(&tio,0,sizeof(tio));
    tio.c_iflag=iflag;
    tio.c_oflag=oflag;
    tio.c_cflag=cflag;
    tio.c_lflag=lflag;
    tio.c_cc[VMIN]=0;
    tio.c_cc[VTIME]=timeout;
    cfsetospeed(&tio,speed);
    cfsetispeed(&tio,speed);
    tty = -1;
  }
  ~SerialPort(){
    Disconnect();
  }

  bool AddFlag(char f, tcflag_t flag){
    if(tty < 0){
      switch(f){
      case 'i': tio.c_iflag |= flag; break;
      case 'o': tio.c_oflag |= flag; break;
      case 'c': tio.c_cflag |= flag; break;
      case 'l': tio.c_lflag |= flag; break;
      default: cerr << "Unknown flag selection " << f << endl; return false;
      }
      return true;
    } else {
      cerr << "Cannot change flags after opening port" <<  endl;
      return false;
    }
  }

  bool Connect(bool verbose, string devStr = string()){
    vector<string> dev;
    if(devStr.size()) dev.push_back(devStr);
    if(dev.size() < 1){
      dev = FindTtyUSB();
      if(dev.size() < 1){
	cerr << "No ttyUSB devices found." << endl;
	return false;
      }
    }
    bool found(false);
    for(unsigned int i = 0; i < dev.size(); i++){
      if(!found){
	tty=open(dev[i].c_str(), O_RDWR);        // O_NONBLOCK might override VMIN and VTIME, so read() may return immediately.
	usleep(500000);
	tcflush(tty, TCIOFLUSH);
	tcsetattr(tty,TCSANOW,&tio);
	usleep(500000);
	tcflush(tty, TCIOFLUSH);

	if(tty < 0){
	  cerr << "Cannot open " << dev[i] << endl;
	  continue;
	}
	if(CheckConnection()){
	  sleep(1);
	  if(verbose) cout << "Found device on " << dev[i] << endl;
	  found = true;
	  break;
	} else {
	  cerr << "Got no good response from device" << endl;
	}
      }
    }
    return found;
  };

  void Disconnect(){
    if(tty >= 0) close(tty);
    tty = -1;
  }

protected:
  virtual bool CheckConnection(){
    return true;
  }

  vector<string> Exchange(string cmd, int wait = 100){
    int n;
    char buf[4096];
    if(verbose) cout << "Sending >" << cmd << "<" << "(" << cmd.size() << ")" << endl;
    cmd += eol;
    tcflush(tty, TCIOFLUSH);
    n = write(tty,cmd.c_str(),cmd.size());
    tcdrain(tty);

    if(n != int(cmd.size()))
      cerr << "Size mismatch: " << n << " != " << cmd.size() << endl;
    else if(verbose)
      cout << "Sent " << n << endl;
    // assert(n == int(cmd.size()));
    usleep(wait*1000);
    n = read(tty,&buf,4095);   // for some reason the actual respone only shows up after another write command
    if(n == 0){
      n = write(tty,eol.c_str(),eol.size());
      tcdrain(tty);
      usleep(wait*1000);
      n = read(tty,&buf,4095);
    }
    tcflush(tty, TCIOFLUSH);

    buf[n] = 0;                  // make sure c-string is terminated

    // std::cout << "Read " << n << std::endl;
    std::istringstream iss(buf);
    vector<string> replies;
    do {
      string s;
      std::getline(iss, s, '\r');
      s.erase(std::remove(s.begin(), s.end(), '\n'), s.end());
      // iss >> s;
      if(s.size()) replies.push_back(s);
    } while(iss.good());

    return replies;
  };

private:
  vector<string> FindTtyUSB(){
    DIR *dp;
    struct dirent *ep;
    vector<string> hits;
    dp = opendir ("/dev/");
    if (dp != NULL)
      {
	while ((ep = readdir (dp))){
	  string f(ep->d_name);
	  if(f.substr(0,6) == string("ttyUSB")){
	    f.insert(0,"/dev/");
	    hits.push_back(f);
	  }
	}
	(void) closedir (dp);
      }
    else
      perror ("Couldn't open the directory");
    return hits;
  }

  struct termios tio;
  int tty, ofile;
  const string eol;
  bool verbose = false;
};
