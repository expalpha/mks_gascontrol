#include "Serial.hh"

class MKS647C: public SerialPort
{
public:
    MKS647C(): SerialPort(IGNBRK, 0, CS8|CREAD|CLOCAL|PARENB|PARODD){};
    bool GetRanges();
    double GetFlow(unsigned short ch);
    vector<double> GetFlows();
    vector<double> GetFlowSetp();
    vector<int> GetStatus();
    bool SetFlow(unsigned short ch, double flow);
protected:
    bool CheckConnection(){
        vector<string> rep;

        rep = Exchange("ID", 200);
        if(rep.size()){
            for(auto r: rep){
                if(r.find("MGC 647C") != string::npos){
                    return true;
                } else if(r.find("E1") == string::npos){
                    cerr << "Unknown response >" << r << "<" << endl;
                } else {
                    std::cout << "Got E1 error, unknown command" << endl;
                }
            }

            for(unsigned int i = 0; i < rep.size(); i++){
                std::cout << i << " > " << rep[i] << std::endl;
            }
            return false;
        } else {
            return false;
        }
    }
private:
    vector<double> full_scale;
    int GetInt(const char *cmd, unsigned short ch);
    bool SetInt(const char *cmd, unsigned short ch, int val);
    const double fl_max[40] =
        {
            1.,        //  0 = 1.000 SCCM
            2.,	      //  1 = 2.000 SCCM
            5.,	      //  2 = 5.000 SCCM
            10.,	      //  3 = 10.00 SCCM
            20.,	      //  4 = 20.00 SCCM
            50.,	      //  5 = 50.00 SCCM
            100.,      //  6 = 100.0 SCCM
            200.,      //  7 = 200.0 SCCM
            500.,      //  8 = 500.0 SCCM
            1.e3,      //  9 = 1.000 SLM
            2.e3,      // 10 = 2.000 SLM
            5.e3,      // 11 = 5.000 SLM
            10.e3,     // 12 = 10.00 SLM
            20.e3,     // 13 = 20.00 SLM
            50.e3,     // 14 = 50.00 SLM
            100.e3,    // 15 = 100.0 SLM
            200.e3,    // 16 = 200.0 SLM
            400.e3,    // 17 = 400.0 SLM
            500.e3,    // 18 = 500.0 SLM
            1000.e3,   // 19 = 1.000 SCMM
            28316.8 * 60. * 1.000,   // 20 = 1.000 SCFH
            28316.8 * 60. * 2.000,   // 21 = 2.000 SCFH
            28316.8 * 60. * 5.000,   // 22 = 5.000 SCFH
            28316.8 * 60. * 10.00,   // 23 = 10.00 SCFH
            28316.8 * 60. * 20.00,   // 24 = 20.00 SCFH
            28316.8 * 60. * 50.00,   // 25 = 50.00 SCFH
            28316.8 * 60. * 100.0,   // 26 = 100.0 SCFH
            28316.8 * 60. * 200.0,   // 27 = 200.0 SCFH
            28316.8 * 60. * 500.0,   // 28 = 500.0 SCFH
            28316.8 * 1.000,	    // 29 = 1.000 SCFM
            28316.8 * 2.000,	    // 30 = 2.000 SCFM
            28316.8 * 5.000,	    // 31 = 5.000 SCFM
            28316.8 * 10.00,	    // 32 = 10.00 SCFM
            28316.8 * 20.00,	    // 33 = 20.00 SCFM
            28316.8 * 50.00,	    // 34 = 50.00 SCFM
            28316.8 * 100.0,	    // 35 = 100.0 SCFM
            28316.8 * 200.0,	    // 36 = 200.0 SCFM
            28316.8 * 500.0,	    // 37 = 500.0 SCFM
            30.e3,		    // 38 = 30.00 SLM
            300.e3		    // 39 = 300.0 SLM
        };
};

int MKS647C::GetInt(const char *cmd, unsigned short ch){
    char buf[64];
    if(strlen(cmd) != 2)
        return -2;
    sprintf(buf, "%s %d R", cmd, ch);
    vector<string> rep = Exchange(buf, 200);
    if(rep.size() == 1)
        return atoi(rep[0].c_str());
    else
        return -1;
}

bool MKS647C::SetInt(const char *cmd, unsigned short ch, int val){
    char buf[64];
    if(strlen(cmd) != 2)
        return -2;
    sprintf(buf, "%s %d %d", cmd, ch, val);
    vector<string> rep = Exchange(buf, 200);
    if(rep.size() == 1){
        if(rep[0].size())
            if(rep[0][0] == 'E'){
                cerr << "Error: " << rep[0] << endl;
                return false;
            }
    }
    return true;
}

bool MKS647C::GetRanges(){
    cout << "Collecting Range settings..." << endl;
    full_scale.clear();
    for(unsigned short ch = 1; ch <= 8; ch++){
        int rng = GetInt("RA", ch);
        float gcf = float(GetInt("GC", ch))*0.01;
        if(rng < 0 || gcf < 0.) return false;
        full_scale.push_back(gcf * fl_max[rng]);
    }
    for(unsigned int i = 0; i < full_scale.size(); i++)
        cout << i+1 << ":\t" << full_scale[i] << endl;
    return true;
}

double MKS647C::GetFlow(unsigned short ch){
    if(full_scale.size() != 8)
        if(!GetRanges()) return -999.;
    double flow = GetInt("FL", ch) * 1.e-3;
    if(true || flow >= 0.)
        return flow * full_scale[ch-1];
    else
        return -999;
}

vector<double> MKS647C::GetFlows(){
    vector<double> flows;
    for(unsigned short ch = 1; ch <= 8; ch++){
        flows.push_back(GetFlow(ch));
    }
    return flows;
}

vector<double> MKS647C::GetFlowSetp(){
    vector<double> flows;
    if(full_scale.size() != 8)
        if(!GetRanges()) return flows;
    for(unsigned short ch = 1; ch <= 8; ch++){
        int flowrb = GetInt("FS", ch);
        flows.push_back(double(flowrb)*1.e-3*full_scale[ch-1]);
    }
    return flows;
}

vector<int> MKS647C::GetStatus(){
    vector<int> status;
    for(unsigned short ch = 1; ch <= 8; ch++){
        status.push_back(GetInt("ST", ch));
    }
    return status;
}

bool MKS647C::SetFlow(unsigned short ch, double flow){
    if(full_scale.size() != 8)
        if(!GetRanges()) return false;
    int flowset = flow/full_scale[ch-1]*1.e3;
    if(SetInt("FS", ch, flowset)){
        int flowrb = GetInt("FS", ch);
        if(flowrb != flowset){
            cerr << "Problem setting flow! Readback " << flowrb << " != " << flowset << endl;
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}
