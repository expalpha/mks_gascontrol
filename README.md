# Control software for MKS Multi-Gas-Controller 647C

Basic interface classes to control an MKS 647C via RS232 and MIDAS.

### Components ###

* serial port interface class
* MKS 647C interface class (inheriting from serial port class)
* *mks_test* test program for  MKS control
* *feMKS* MIDAS frontend

### Usage ###

This code does not access all settings of the MKS 647C. This is partly by design, so parameters regarding gas composition and sensor
range have to be made in person on the device, after confirming with the actual physical hardware.