# Makefile

CFLAGS += -O2 -g -std=c++11
CFLAGS += -Wall -Wuninitialized

LIBS += -lpthread
LIBS += -lm

CFLAGS += -I$(MIDASSYS)/include -I$(MIDASSYS)/mvodb
LIBS   += -L$(MIDASSYS)/linux-armv7l-remoteonly/lib -lmidas -lrt -lutil

all:: feMKS.exe mks_test.exe

%.o: %.cxx
	g++ -c -o $@ $(CFLAGS) $<

%.exe: %.o
	$(CXX) -o $@ $^ $(CFLAGS) $(LIB) $(LDFLAGS) $(LIBS)


clean::
	-rm -f *.o *.exe

#end
