#include "MKS647C.hh"

int main(int argc, char** argv){
    MKS647C mks;
    // mks.Connect(true, "/dev/ttyUSB0");
    mks.Connect(true);

    mks.GetRanges();

    cout << "Flows:";
    for(int i = 1; i <= 8; i++)
      cout << '\t' << i << ": " << mks.GetFlow(i);
    cout << endl;

    bool success = mks.SetFlow(2, 0.5);
    cout << "mks.SetFlow(2, 0.5)): ";
    if(success) cout << "OK!" << endl;
    else cout << "Failed!" << endl;

    success = mks.SetFlow(2, 999.);
    cout << "mks.SetFlow(2, 999.)): ";
    if(success) cout << "OK!" << endl;
    else cout << "Failed!" << endl;

    cout << "Flows:";
    for(int i = 1; i <= 8; i++)
      cout << '\t' << i << ": " << mks.GetFlow(i);
    cout << endl;

    return 0;
}
